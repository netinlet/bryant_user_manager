lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'user_manager/json_file_store'
require 'user_manager/role'
require 'user_manager/user'
require 'user_manager/user_table_decorator'
require 'user_manager/cli'


module UserManager

end
