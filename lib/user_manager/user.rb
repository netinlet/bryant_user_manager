require 'digest'

module UserManager
  class User
    include JsonFileStore

    persist_fields :username, :encrypted_password, :salt, :roles

    def id
      self.username
    end

    def roles
      @roles ||= []
    end

    def add_roles(*roles)
      roles.each{|r| self.roles << r unless self.roles.include?(r) || !UserManager::Role.valid_role?(r)}
    end
    alias :add_role :add_roles

    def remove_roles(*roles)
      roles.each{|r| self.roles.delete(r)}
    end
    alias :remove_role :remove_roles

    # Updates and encrypts the password. Uses a new salt for every password change
    def password=(new_password)
      self.salt = rand
      self.encrypted_password = encrypt_password(self.salt, new_password)
    end

    # validates password is valid
    def password_valid?(passwd)
      encrypt_password(self.salt, passwd) == self.encrypted_password
    end

    # does the user have a particular role
    def has_role?(role_name)
      self.roles.include? role_name
    end

    # shortcut methods for asking if a user has a role - is_admin?  is_manager?  is_user?
    UserManager::Role.all.each do |role_name|
      define_method "is_#{role_name}?" do
        self.has_role?(role_name)
      end
    end
    
    protected

    def encrypt_password(salt, passwd)
      Digest::SHA512.hexdigest("#{passwd}-#{salt}")
    end

  end
end
