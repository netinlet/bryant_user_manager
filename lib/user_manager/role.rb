module UserManager
  class Role
    
    ROLES = %w{admin manager user}
    
    #
    # Returns a list of all roles  - cloned so as not to modify the constant
    #
    def self.all
      ROLES.clone
    end
    
    def self.valid_role?(role_name)
      ROLES.include?(role_name)
    end
  end
end