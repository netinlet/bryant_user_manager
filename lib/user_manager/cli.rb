module UserManager
  class Cli

    CMD_HELP = {
      "list" => "usrmgr list - lists all users",
      "show" => "usrmgr show <username> - shows a specific user",
      "set_password" => "usrmgr set_password <username> <new_password> - updates a users password",
      "add_role" => "usrmgr add_role <username> <new_role> - adds a role to the specified user",
      "rm_role" => "usrmgr rm_role <username> <role> - removes the role from the specified user",
      "help" => "Prints out how to use this program"
    }

    COMMANDS = CMD_HELP.keys

    def execute(command, *args)
      processed_args = remove_nil_from_array(args) # kept passing a [nil] value into the execute method for no additional commands
      if CMD_HELP.has_key?(command) && self.respond_to?(command.to_sym) && self.method(command.to_sym).arity == processed_args.size
        if processed_args.any?
          self.send(command, *args)
        else
          self.send(command)
        end
      else
        puts "Error: No Such Command or improper use of command"
        puts
        print_help
      end
    end

    def list
      print_table *UserManager::User.all
    end

    def show(username)
      print_table UserManager::User.find(username)
    end

    def set_password(username, password)
      if user = UserManager::User.find(username)
        user.password = password
        user.save
        puts "Password Successfully Updated for #{username}"
      else
        puts "Error: No Such User"
        puts
        print_help
      end

    end

    def add_role(username, role)
      if user = UserManager::User.find(username)
        user.add_roles(role)
        user.save
        puts "Added #{role} to #{username}"
      else
        puts "Error: No Such User"
        puts
        print_help
      end
    end

    def rm_role(username, role)
      if user = UserManager::User.find(username)
        user.remove_roles(role)
        user.save
        puts "Removed #{role} from #{username}"
      else
        puts "Error: No Such User"
        puts
        print_help
      end

    end

    def print_help
      puts "Usage: usrmgr <command> <command_args>"
      puts "Commands:"
      CMD_HELP.each do |cmd, help|
        puts "    #{cmd} - #{help}"
      end
      puts
    end
    alias :help :print_help

    def print_table(*users)
      users = remove_nil_from_array(users)
      if users.empty?
        puts "No Such User"
        return
      end
      users = decorate_users(*users)
      column_names = users.first.to_column_names
      data = users.map{|user| user.to_data}
      puts Ruport::Data::Table.new(:column_names => column_names, :data => data).to_s
    end

    protected
      def decorate_users(*users)
        users.map{|user| user.extend UserManager::UserTableDecorator }
      end

      def remove_nil_from_array(a)
        a.reject{|a| a.nil?}
      end
  end
end
