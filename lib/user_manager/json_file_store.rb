require 'json'

#
# Simple, reusable JSON file store implementation
#
module UserManager
  module JsonFileStore

    def self.included(receiver)
      receiver.extend ClassMethods
      receiver.send(:include, InstanceMethods)
    end

    module ClassMethods
      
      # The directory the users file will be persisted.
      # Defaults to the db directory in the project root
      # ex. MyClass.storage_location
      def storage_location
        @storage_root || set_storage_location(File.expand_path(File.dirname(__FILE__) + '/../../db'))
      end
      
      # Set the storage location.  Optional second argument is to create the storage directory if it doesn't already exist
      # ex. MyClass.set_storage_location('/path/to/nowhere/')
      def set_storage_location(database_directory, create_dir = true)
        full_db_path = File.expand_path(database_directory) # just in case full path isn't specified
        if (create_dir && !File.directory?(database_directory))
          Dir.mkdir(full_db_path)
        end
        # check to make sure valid
        unless File.directory?(database_directory)
          raise RuntimeError, "JsonFileStore Storage Location[#{database_directory}] does not exist or is a file"
        end
        @storage_root = full_db_path
      end
      
      # the filename of the datastore
      def db_store
        "#{storage_location}/#{to_s}.json"
      end
      
      # the datastore database - in this case, a simple hash
      def db
        @db ||= begin
          if File.exists?(db_store)
            restore(File.read(db_store))
          else
            {}
          end
        end
      end
      
      # reload the database from file, loosing all in-memory modified data
      def reload_db
        @db = nil
        db
      end
      
      # persist the database
      def store
        File.open(db_store, "w"){|file| file << JSON.pretty_generate(db)}
      end
      
      # Search for a record in the database by primary key
      # Ex. MyClass.find(primary_key)
      def find(id)
        if db.has_key?(id)
          restore(db[id])
        else
          nil
        end
      end
      
      # return a list of all records
      def all
        db.values.map{|data| restore(data)}
      end

      # Create attr_accessors and store the list of persistent fields for later reference
      def persist_fields(*fields)
        self.send(:attr_accessor, *fields)
        @persistent_fields = fields
      end
      
      # list of persistent fields
      def persistent_fields
        @persistent_fields ||= []
      end
      
      # Unmarshall JSON data
      def json_create(serialized_data)
        new(serialized_data['attributes'])
      end

      private

      def restore(from_json)
        JSON.parse(from_json, :create_additions => true)
      end
    end

    module InstanceMethods

      def initialize(attributes = {})
        attributes.each{|key,value| self.send("#{key}=", value)}
      end

      def id
        raise RuntimeError, "Must implement 'id' in your concrete class to persist records"
      end
      
      # Ask a record to save itself
      # Ex. @my_class.save
      def save
        self.class.db[self.id] = self.to_json
        self.class.store
      end
      
      def as_json(*)
        {
          JSON.create_id => self.class.name,
          'attributes' => self.to_attributes
        }
      end

      def to_json(*)
        as_json.to_json
      end

      def ==(another_object)
        return false unless another_object.kind_of?(self.class) && !another_object.nil?
        fields_same_table = self.class.persistent_fields.map do |field|
          self.send(field) == another_object.send(field)
        end

        fields_same_table.detect{|n| false == n}.nil?
      end
      
      protected
        def to_attributes
          result = {}
          self.class.persistent_fields.map{|field| result[field.to_s] = self.send(field)}
          result
        end
    end
  end
end
