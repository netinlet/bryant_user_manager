# 
# Class which adds functionality to a User to gather the proper data for formatting table
#
module UserManager
  module UserTableDecorator

    # Returns the columns names for the table being built
    def to_column_names
      ["Username", "Is Admin", "Is Manager", "Is User"]
    end
    
    # returns the data mapping out to the columns
    def to_data
      [
        self.username,
        cond_print(self.is_admin?,  "x", ""),
        cond_print(self.is_manager?,"x", ""),
        cond_print(self.is_user?,   "x", "")
      ]
    end

    protected
    # Returns set result based on input boolean value
    def cond_print(boolean_value, if_true, if_false)
      boolean_value ? if_true : if_false
    end
  end
end