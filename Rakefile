# encoding: utf-8

lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

require 'rubygems'
require 'bundler'
begin
  Bundler.setup(:default, :development)
rescue Bundler::BundlerError => e
  $stderr.puts e.message
  $stderr.puts "Run `bundle install` to install missing gems"
  exit e.status_code
end
require 'rake'

require 'rspec/core'
require 'rspec/core/rake_task'
RSpec::Core::RakeTask.new(:spec) do |spec|
  spec.pattern = FileList['spec/**/*_spec.rb']
end

RSpec::Core::RakeTask.new(:coverage) do |spec|
  ENV['COVERAGE'] = '1'
  Rake::Task["spec"].execute
end

task :default => :spec

require 'rdoc/task'
Rake::RDocTask.new do |rdoc|
  version = File.exist?('VERSION') ? File.read('VERSION') : ""

  rdoc.rdoc_dir = 'rdoc'
  rdoc.title = "user_manager #{version}"
  rdoc.rdoc_files.include('README*')
  rdoc.rdoc_files.include('lib/**/*.rb')
end

desc "Recreate User Data"
task :seed_users do
  require File.expand_path 'lib/user_manager'
  FileUtils.rm_r UserManager::User.db_store, :force => true
  pete = UserManager::User.new(:username => 'pete', :roles => %w{admin manager user})
  abhi = UserManager::User.new(:username => 'abhi', :roles => %w{manager user})
  doug = UserManager::User.new(:username => 'doug', :roles => %w{user})
  pete.password = 'test'
  pete.save
  abhi.password = 'test'
  abhi.save
  doug.password = 'test'
  doug.save
end
