require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe UserManager::Cli do
  
  before(:each) do
    @cli = UserManager::Cli.new
  end
  
  it "correctly executes the command list" do
    @cli.should_receive(:send).with("list")
    @cli.execute "list"
  end

  it "correctly executes the command show" do
    @cli.should_receive(:send).with("show", "doug")
    @cli.execute "show", "doug"
  end

  it "correctly executes the command set_password" do
    @cli.should_receive(:send).with("set_password", "doug", "test")
    @cli.execute "set_password", "doug", "test"
  end

end
