require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe UserManager::JsonFileStore do

  class PersistableObject
    include UserManager::JsonFileStore
    persist_fields :name, :age

    def id
      self.name
    end

  end

  class UnpersistableObject
    include UserManager::JsonFileStore
  end

  before(:each) do

  end

  context "ClassMethods" do

    before(:each) do
      @target_storage_dir = tmp_storage_dir("json_file_store_test")
      PersistableObject.set_storage_location @target_storage_dir
    end

    after(:each) do
      @target_storage_dir = tmp_storage_dir("json_file_store_test")
      FileUtils.rm_r @target_storage_dir, :force => true
    end

    it "has a default storage location" do
      PersistableObject.instance_variable_set :@storage_root, nil # reset the default location
      PersistableObject.storage_location.should eq(File.expand_path(File.dirname(__FILE__) + '/../../db'))
    end

    it "can assign a specific storage location" do
      PersistableObject.storage_location.should eq(@target_storage_dir)
    end

    it "creates the storage location directory if it doesn't exist with default arguments" do
      Dir.delete(@target_storage_dir) if File.exists?(@target_storage_dir)
      File.exists?(@target_storage_dir).should be_false
      PersistableObject.set_storage_location @target_storage_dir
      File.exists?(@target_storage_dir).should be_true
    end

    it "fails to set the storage directory when the storage directory doesn't exist and create_dir flag is set to false" do
      Dir.delete(@target_storage_dir) if File.exists?(@target_storage_dir)
      File.exists?(@target_storage_dir).should be_false
      lambda{PersistableObject.set_storage_location(@target_storage_dir, false)}.should raise_error(RuntimeError)
    end

    it "fails to set the storage directory when the directory already exists as a file" do
      Dir.delete(@target_storage_dir) if File.exists?(@target_storage_dir)
      File.open(@target_storage_dir, "w"){|f| f << "RSPEC TEST"}
      lambda{PersistableObject.set_storage_location(@target_storage_dir, false)}.should raise_error(RuntimeError)
      File.delete(@target_storage_dir)
    end

    it "knows the location of the db store" do
      PersistableObject.db_store.should eq(@target_storage_dir + "/PersistableObject.json")
    end

    it "loads what is stored in the database file with a db call" do
      File.open(PersistableObject.db_store, "w"){|file| file << {"foo" => %w{one two three} }.to_json }
      PersistableObject.reload_db
      PersistableObject.db.should eq({"foo" => %w{one two three}})
    end

    it "stores the contents of the db" do
      PersistableObject.reload_db
      PersistableObject.db["foo"] = "doug"
      PersistableObject.store
      JSON.parse(File.read(PersistableObject.db_store)).should eq({"foo" => "doug"})
    end

    it "resets the db to empty with reload_db" do
      PersistableObject.reload_db
      PersistableObject.db.should eq({})
    end

    it "deserializes objects correctly" do
      obj = PersistableObject.new
      obj.name = 'Bubba'
      obj.age = '87'
      raw_data = obj.to_json
      JSON.parse(raw_data, :create_additions => true).should eq(obj)
    end

  end

  context "InstanceMethods" do
    before(:each) do
      @storage_dir = tmp_storage_dir("json_file_store_instance_tests")
      PersistableObject.set_storage_location(@storage_dir)
      @persistable = PersistableObject.new
      @unpersistable = UnpersistableObject.new
    end

    after(:each) do
      @storage_dir = tmp_storage_dir("json_file_store_instance_tests")
      FileUtils.rm_r @storage_dir, :force => true
    end

    it "raises a RuntimeError if 'id' is not overridden" do
      lambda{@unpersistable.id}.should raise_error(RuntimeError, "Must implement 'id' in your concrete class to persist records")
    end

    it "has an empty list of persistent fields when empty" do
      UnpersistableObject.persistent_fields.should eq([])
    end

    it "can store a list of persistent fields" do
      PersistableObject.persistent_fields.should eq([:name, :age])
    end

    it "creates accessors for persistent fields" do
      obj = PersistableObject.new
      obj.should respond_to(:name)
      obj.should respond_to(:age)
    end

    it "properly serializes an object in the correct format" do
      obj = PersistableObject.new
      obj.name = 'Bubba'
      obj.age = '87'
      obj.to_json.should eq({'json_class' => 'PersistableObject', 'attributes' => {'name' => 'Bubba', 'age' => '87'}}.to_json)
    end

    it "should properly compare object equality for equal objects" do
      a = PersistableObject.new
      a.name = 'Bubba'
      a.age = '87'

      b = PersistableObject.new
      b.name = 'Bubba'
      b.age = '87'

      a.should == b
    end

    it "should report two different objects as not equal" do
      a = PersistableObject.new
      a.name = 'Smith'
      a.age = '87'

      b = PersistableObject.new
      b.name = 'Bubba'
      b.age = '87'

      a.should_not == b
    end

    it "should handle nil object comparison" do
      a = PersistableObject.new
      a.name = 'Smith'
      a.age = '87'
      a.should_not == nil
    end

    it "should handle different type object comparison" do
      a = PersistableObject.new
      a.name = 'Smith'
      a.age = '87'
      a.should_not == %w{foo bar baz}
    end

    it "is able to persist a record" do
      a = PersistableObject.new
      a.name = 'Smith'
      a.age = '87'
      a.save
      PersistableObject.reload_db
      JSON.parse(PersistableObject.db["Smith"], create_additions: true).should eq(a)
    end

    it "is able to find a record in the db which exists" do
      a = PersistableObject.new
      a.name = 'Smith'
      a.age = '87'
      a.save

      PersistableObject.find("Smith").should eq(a)
    end

    it "return nil if it can't find an object in the database" do
      PersistableObject.find("foo").should be_nil
    end

    it "can return an instantiated list of all the objects in the db" do
      PersistableObject.db.clear
      entries = {}

      # add three entries to the db
      %w{Ben Nicky Valentino}.each do |name|
        a = PersistableObject.new
        a.name = name
        a.age = '87'
        a.save
        entries[a.id] = a
      end
      PersistableObject.reload_db # make sure we are fresh and pulling from serialized data
      found = PersistableObject.all
      found.size.should eq(3)
      found[0].should eq(entries[found[0].name])
      found[1].should eq(entries[found[1].name])
      found[2].should eq(entries[found[2].name])
    end

  end

end
