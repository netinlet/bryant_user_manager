require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe UserManager::Role do
  
  it "has a list of available roles accessable via 'all'" do
    UserManager::Role.all.should eq(%w{admin manager user})
  end
  
  UserManager::Role.all.each do |role_name|
    it "reports #{role_name} is a valid role" do
      UserManager::Role.valid_role?(role_name).should be_true
    end
  end
  
  it "reports foo is not a valid role" do
    UserManager::Role.valid_role?('foo').should be_false
  end
  
end
