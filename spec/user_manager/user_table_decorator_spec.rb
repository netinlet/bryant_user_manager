require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe UserManager::UserTableDecorator do
  
  before(:each) do
    @user = UserManager::User.new
    @user.username = 'rspec-table-test'
    @user.roles = %w{manager user}
    @user.extend UserManager::UserTableDecorator
  end
  
  it "returns the correct columns for the user table view" do
    @user.to_column_names.should eq(["Username", "Is Admin", "Is Manager", "Is User"])
  end
  
  it "returns the properly formatted data for the user table view" do
    @user.to_data.should eq(['rspec-table-test', "", "x", "x"])
  end
  
end
