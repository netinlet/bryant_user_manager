require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe UserManager::User do

  after(:each) do
    FileUtils.rm_r tmp_storage_dir("user_manager_spec"), :force => true
  end

  before(:each) do
    UserManager::User.set_storage_location tmp_storage_dir("user_manager_spec")
    @user = UserManager::User.new
    @user.username = 'rspec'
    @user.password = 'test'
    @user.roles = UserManager::Role.all
  end

  it "should have an accessor for the field :username" do
    @user.should respond_to :username
  end

  it "should have an accessor for the field :encrypted_password" do
    @user.should respond_to :encrypted_password
  end

  it "should have an accessor for the field :roles" do
    @user.should respond_to :roles
  end

  it "should have an accessor for the field :salt" do
    @user.should respond_to :salt
  end

  it "Reports a user has a role when they do" do
    @user.has_role?('admin').should  be_true
  end

  it "Reports a user does not have a role when they don't" do
    @user.has_role?('foo').should  be_false
  end

  it "can check admin permissions with is_admin?" do
    @user.is_admin?.should be_true
  end

  it "can check manager permissions with is_manager?" do
    @user.is_manager?.should be_true
  end

  it "can check admin permissions with is_user?" do
    @user.is_user?.should be_true
    @user.remove_role 'user'
    @user.is_user?.should be_false
  end

  context "Roles and Users" do
    before(:each) do
      @user = UserManager::User.new
      @user.username = 'password_handling'
    end

    it "can add a single role" do
      @user.add_role "admin"
      @user.is_admin?.should be_true
      @user.is_manager?.should be_false
      @user.is_user?.should be_false
    end

    it "can add multiple roles" do
      @user.add_roles "admin", "manager"
      @user.is_admin?.should be_true
      @user.is_manager?.should be_true
      @user.is_user?.should be_false
    end

    it "can remove a single role" do
      @user.add_roles "admin", "manager", "user"
      @user.remove_role "manager"
      @user.is_admin?.should be_true
      @user.is_manager?.should be_false
      @user.is_user?.should be_true
    end

    it "can remove multiple roles" do
      @user.add_roles "admin", "manager", "user"
      @user.remove_roles "manager", "admin"
      @user.is_admin?.should be_false
      @user.is_manager?.should be_false
      @user.is_user?.should be_true
    end

    it "does not add roles which are invalid" do
      @user.add_roles 'foo'
      @user.has_role?('foo').should be_false
    end

  end


  context "Password Handling" do
    before(:each) do
      @user = UserManager::User.new
      @user.username = 'password_handling'
    end

    it "encrypts the password upon setting the password" do
      @user.encrypted_password.should be_nil
      @user.password = 'test'
      @user.encrypted_password.should_not be_nil
    end

    it "is capable of verifying a correct password for the user" do
      @user.password = 'test'
      @user.password_valid?('test').should be_true
    end
    it "is capable of rejecting an incorrect password for the user" do
      @user.password = 'test'
      @user.password_valid?('password').should be_false
    end

    it "is capable of updating a record without upsetting the password" do
      @user.password = 'test'
      @user.save
      UserManager::User.reload_db
      UserManager::User.find("password_handling").password_valid?('test').should be_true
      @user.roles = UserManager::Role.all[0..1]
      @user.save
      UserManager::User.reload_db
      found_user = UserManager::User.find("password_handling")
      found_user.roles.should eq(UserManager::Role.all[0..1])
      found_user.username.should eq("password_handling")
      found_user.password_valid?('test').should be_true
    end

  end

end
